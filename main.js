const axios = require("axios");

// Edit accordingly
const preferences = {
  token: "<your-access-token-here>", // Get your own here https://gitlab.com/-/profile/personal_access_tokens
  baseUrl: "https://gitlab.com/api/v4",
  visibility: "public", // or public
};

// From this point onwards, you don't have to edit anything...
//...
// Getting all of your projects
console.log("Fetching your projects...");
axios
  .get(
    `${preferences.baseUrl}/projects?owned=true&visibility=${preferences.visibility}`,
    {
      headers: {
        Authorization: `Bearer ${preferences.token}`,
      },
    }
  )
  .then(async function (res) {
    // Mapping the project ids
    let ids = res.data.map((e) => e.id);
    console.log(ids);

    // Deleting everything
    for (let id of ids) {
      await axios.delete(`${preferences.baseUrl}/projects/${id}/`, {
        headers: {
          Authorization: `Bearer ${preferences.token}`,
        },
      });
      console.log("Successfully deleted", id);
    }

    console.log("Done!~");
  })
  .catch(function (error) {
    // If something bad happens
    console.log("Uh oh... It says");
    console.log(error);
  });
